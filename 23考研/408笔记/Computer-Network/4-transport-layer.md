# 传输层

## 基本概念

传输层只有主机才有，而路由器这种中间设备至多只有物理层、数据链路层和网络层三层架构。

是面向通信的最高层，也是用户功能的最底层。

### 传输层的功能

1. 传输层提供进程与进程之间的逻辑通信。
2. 复用与分用。（复用：应用层所有的应用进程都可以通过传输层再传输到网络层；分用：传输层从网络层收到数据后交付指明的应用进程）
3. 差错检测。

### 寻址与端口

+ 端口（逻辑端口/软件端口）：是传输层的$SAP$，标识主机中的应用进程。
+ 每一个端口有用于区分的端口号，只有本地意义，因特网中不同主机的相同端口号无联系。
+ 端口号长度为$16bit$，能标识$65536$个不同的端口号。
+ 端口号按范围划分可以分为：
  + 服务端使用的端口号：
    + 熟知端口号（$0-1023$）：给$TCP/IP$最重要的一些应用程序，让所有用户都知道。
    + 登记端口号（$1024-49151$）：为没有熟知端口号的应用程序使用。
  + 客户端使用的端口号：仅在客户进程运行时才系统动态分配。
+ 套接字$Socket$=(主机$IP$地址,端口号)。唯一标识了网络中的一个主机和它上面的一个进程。如果有新的同样套接字的连接请求建立，则建立失败，不影响原有连接。

常用的端口号：

应用程序|熟知端口号
:-----:|:------:
FTP数据|20
FTP控制|21
TELNET|23
SMTP|25
DNS|53
TFTP|69
HTTP|80
POP3|110
SNMP|161
HTTPS|443

## UDP协议

用户数据报协议只在$IP$数据报服务之上添加了两个功能，即只有复用分用（接收方的传输层剥去报文首部后，能把这些数据正确交付到目的进程。通过目的端口实现）与差错检测功能。

### 主要特点

1. 无连接，减少开销和发送数据之前的时延。
2. 使用最大努力交付，而非保证可靠交付。所以不会对报文编号。
3. 面向报文（不对报文拆分，应用层给多长报文，$UDP$就会照样一次发送一个完整报文），适合一次性传输少量数据的网络应用。
4. 无拥塞控制，适合很多实时应用，实时应用延迟要求高，需要立即响应。
5. 支持一对一、一对多、多对一、多对多的交互通信。
6. 首部开销小，$8B$，而$TCP$需要$20B$。

因为$UDP$不保证可靠性，所以其可靠性由应用层完成。

### UDP数据报格式

![UDP数据报格式][UDPformat]

+ 源端口号：需要对方回应时选用，如果不需要回应，可以不填，即是全$0$的。
+ 目的端口号：是必填的。分用时，如果找不到对应的目的端口号就丢弃该报文，并向发送方发送$ICMP$端口不可达差错报告报文。
+ $UDP$长度：整个$UDP$用户数据报的长度，首部加上数据部分。最小值为$8$。以字节为单位。不包括伪首部。
+ $UDP$检验和：检测整个$UDP$数据报是否有错（伪首部+首部+数据，而$IP$只检测首部），错就丢弃。若不想校验则是全$0$。若计算结果为全$0$则置为全$1$。

### UDP协议校验和

具体的$UDP$数据报格式如下：

![UDP具体格式][UDPspecificformat]

+ 伪首部只有在计算检验和时才出现，不向下传达也不向上提交。
+ 其中的$17$代表封装$UDP$报文的$IP$数据报首部协议字段是$17$。
+ $UDP$长度是$UDP$首部$8B$加上数据长度，不包括伪首部。

发送端：

如果不使用校验和字段，则字段全部填充$0$。

1. 填上$12B$的伪首部。
2. 全$0$填充检验和字段。
3. $UDP$数据报要看成许多$16$位的字符串连接一起，全$0$填充数据部分末尾，使数据报成为偶数个字节。
4. 伪首部+首部+数据部分采用二进制反码求和。
5. 二进制反码运算求和再取反填入校验和字段。
6. 去掉伪首部进行发送。

接收端：

如果校验和字段计算结果恰好为$0$，则表示错误，字段全部填充$1$。

1. 填上伪首部，若不是偶数个字节还要在末尾加$0$。
2. 伪首部+首部+数据部分采用二进制反码求和。
3. 如果结果全为$1$则无差错，否则出错，丢弃或交给应用层并附上出错的警告。

![UDP校验][UDPcheck]

## TCP协议

传输控制协议对比用户数据报协议更复杂。

### TCP协议主要特点

1. 是面向连接（虚连接）的传输层协议。
2. 每一条$TCP$连接只能有两个端点，所以连接是一对一的。
3. 提供可靠有序的服务，无差错不重复（使用确认机制）。
4. 提供全双工通信，包含发送缓存（准备发送的数据与已放送但是未确认的数据）与接受缓存（按序到达但是未被读取的数据与不按序到达的数据）。
5. 面向字节流。$TCP$把应用程序交下来的数据看成一连串的无结构字节流。

### TCP数据报格式

$TCP$传输的数据单位称为报文段。可以用来传输数据，也可以用来建立连接、释放连接、应答。长度为$4B$整数倍，默认最短为$20B$，报头最长为$60B$。

![TCP报文段格式][TCPformat]

+ 源端口和目的端口：各$2B$。
+ 序号：在一个$TCP$连接中传送的字节流中的每一个字节都按顺序编号，本字段表示本报文段所发送数据的第一个字节的序号。范围为$0\sim2^{32}-1$。
+ 确认号：期望收到对方下一个报文段的第一个数据字节的序号。若确认号为$N$，则证明到序号$N-1$为止的所有数据都已正确收到。
+ 数据偏移（首部长度）：$TCP$报文段的数据起始处距离$TCP$报文段的起始处有多远，即$TCP$报头的长度。以$4B$位单位，即$1$个数值是$4B$，最大值为$15$，达到$TCP$首部的最大值$60B$。
+ 保留：目前为$0$。

还有六个控制位，除了$PSH$和$RST$位都较重要：

1. 紧急位$URG$：$URG=1$时， 标明此报文段中有紧急数据，是高优先级的数据，应尽快传送，不用在缓存里排队。紧急数据都在数据报最前面，配合紧急指针字段使用，即数据从第一个字节到紧急指针所指字节之间的数据就是紧急数据。
2. 确认位$ACK$：$ACK=1$时确认号有效，在连接建立后所有传送的报文段都必须把$ACK$置为$1$。
3. 推送位$PSH$：$PSH=1$时，接收方尽快交付接收应用进程，不再等到缓存填满再向上交付。如果没有$PSH$，一般都是接收方缓存满了之后再将数据发送到主机。
4. 复位$RST$：$RST=1$时， 表明$TCP$连接中出现严重差错，必须释放连接，然后再重新建立传输链接。
5. 同步位$SYN$：$SYN=1$时，表明是一个连接请求/连接接受报文。此时若$ACK=0$代表这是一个连接请求报文，若$ACK=1$代表这是一个连接接收报文。
6. 终止位$FIN$：$FIN=1$时，表明此报文段文送方数据已发完，要求释放连接。

+ 窗口：指的是发送本报文段的一方的接收窗口，即现在允许对方还可以发送的数据量，防止对方发送过多数据导致自己无法接受。占$2B$，范围$0\sim2^{16}-1$。
+ 检验和：检验首部+数据，检验时要加上$12B$伪首部，第四个的协议字段由$17$改为$6$。
+ 紧急指针： $URG=1$时才有意义，指出本报文段中数据部分的紧急数据的字节数。
+ 选项：最大报文段长度$MSS$、窗口扩大、时间戳、选择确认……
+ 填充：当首部长度不为$4$的整数倍就由填充部分填充$0$，到$4$字节的整数倍。

### TCP协议连接管理

TCP建立连接采用客户服务器方式。但是实际上任何一台计算机都可能做服务器也可能做客户端。

#### TCP三次握手（建立连接）

![TCP建立连接][TCPbuildlink]

1. 最开始客户端与服务端都是关闭状态。
2. 服务器端创建传输控制块$TCB$，进入收听状态准备接受连接请求。
3. 客户端创建$TCP$，发送请求连接报文段，无应用层数据。然后客户端进入同步已发送状态。
4. 服务端接受报文段后进入同步收到状态，服务器端为该$TCP$连接分配缓存和变量，并向客户端返回确认报文段，允许连接，无应用层数据。
5. 客户端接受报文后变成已建立连接状态，为该$TCP$连接分配缓存和变量，并向服务器端返回确认的确认，可以携带数据。
6. 服务端接受到报文段后变成已建立连接状态。

注释：其中$seq$表示序号，指本报文的随机编号；$ack$表示确认号，指期待对方发送的报文的第一个序号。

若不指出为$1$，则代表其值为$0$。

1. 第一部分：
   + $SYN=1$：主机$A$要建立连接了。
   + $seq=x$（随机）：后面没有数据。
2. 第二部分：
   + $SYN=1$：主机$B$同意主机$A$建立连接。
   + $ACK=1$：连接确认建立了，之后的$ACK$必须都置为$1$，表示开始同步。
   + $seq=y$（随机）：设置初始序号，后面没有数据。
   + $ack=x+1$：表示期待对方放松的报文段的第一个字节，之前发送方$A$说发送的是第$x$位数据（虽然发送方是任意给出的），所以主机$B$要的是$x+1$位数据。
3. 第三部分：
   <!-- + $SYN=0$：$SYN$只有在建立连接时才为$1$，其他时候均设为$0$。 -->
   + $ACK=1$：连接建立了，之后的$ACK$必须都置为$1$。
   + $seq=x+1$：主机$A$发送的报文段的第一个字节就是$x+1$。
   + $ack=y+1$：之前接收方$B$发送的是第$y$位数据（虽然接收方是任意给出的），所以主机$A$要的是$y+1$位数，对其确认。

值得注意的是$seq$的值是随机的，所以客户端的和服务器端的序列值可能相同

#### SYN洪泛攻击

由于三次握手时服务器的资源在第二次握手时分配，客户端自愿者第三次握手时分配，可能导致反复确认与占用，产生$SYN$洪泛攻击。$SYN$洪泛攻击发生在$OSI$第四层。

这种方式利用$TCP$协议的特性，就是三次握手。攻击者发送$TCP$的$SYN$包，$SYN$是$TCP$三次握手中的第一个数据包，即第一步，而当服务器返回$ACK$后，该攻击者不对其进行再确认，那这个$TCP$连接就处于挂起状态，也就是所谓的半连接状态，服务器收不到再确认的话，还会重复发送$ACK$给攻击者。这样更加会浪费服务器的资源。攻击者对服务器发送非常大量的这种$TCP$连接，由于每一个都没法完成三次握手，所以在服务器上，这些$TCP$连接会因为挂起状态而消耗$CPU$和内存，最后服务器可能死机，就无法为正常用户提供服务了。可以通过设置$SYN\,Cookies$来解决。

#### TCP四次挥手（连接释放）

每一条$TCP$连接的两个进程中的任何一个都能终止连接，连接结束后主机的资源将被释放。

![TCP释放连接][TCPreleaselink]

1. 最开始客户端与服务端都是已建立连接状态。
2. 客户端发送连接释放报文段，停止发送数据，主动关闭$TCP$连接，进入终止等待$1$状态。
3. 服务端会回送一个确认报文段，此时服务器端进入关闭等待状态，客户到服务器这个方向的连接就释放了——半关闭的状态，不允许客户端再发送数据给服务器。
4. 客户端接受报文段后进入终止等待$2$状态。
5. 服务器发完数据，如果没有要向服务器发送的数据，就发出释放连接报文段，主动关闭$TCP$连接，进入最后确认阶段。
6. 客户端回送一个确认报文字段，服务器端接收后进入关闭状态。客户端等到时间等待计时器设置的$2MSL$（最长报文段寿命）后彻底关闭连接，关闭服务器到客户这个方向，进入关闭状态。

注释：

1. 第一部分：
   + $FIN=1$：主机$A$要释放连接。
   + $seq=u$（随机）：后面可以有数据也可以没有数据。
2. 第二部分：
   + $ACK=1$：连接建立了，之后的$ACK$必须都置为$1$。
   + $seq=v$（随机）：$v=u+$第一部分数据长度$+1$，如果第一部分的确认报文没有数据就是$v=u+1$。
   + $ack=u+1$：之前发送方$A$发送的是第$u$位数据，所以主机$B$要的是$u+1$位数据（尽管此时$A$已经决定释放连接了）。
3. 第三部分：
   + $FIN=1$：主机$B$要释放连接。
   + $ACK=1$：连接建立了，之后的$ACK$必须都置为$1$。
   + $seq=w$（随机）：$w=v+$第二部分数据长度$+1$，如果第二部分的确认报文没有数据就是$w=v+1$。
   + $ack=u+1$：之前发送方$A$说发送的是第$u$位数据，所以主机$B$要的是$u+1$位数据（因为A直接不发数据了，所以第二段第三段的$ack$都是$u+1$）。
4. 第四部分：
   + $ACK=1$：连接建立了，之后的$ACK$必须都置为$1$。
   + $seq=u+1$：之前发的数据时第$u$位数据，$B$也要第$u+1$位数据，所以我发第$u+1$位数据。
   + $ack=w+1$：之前发送方$B$说发送的是第$w$位数据，所以主机$A$要的是$w+1$位数据。

为什么要等待$2MSL$时间？

1. 保证$A$发送的最后一个$ACK$报文段能发送到$B$，否则$B$服务器会接收不到$A$确认的信息，而$A$已经关闭无法重发确认报文段，从而$B$无法正常关闭。
2. 防止已失效的连接请求报文段传输到下一次的连接请求，干扰下一次的连接服务。

### TCP协议可靠传输

传输层使用的是$GBN$与$SR$的混合。

#### 校验

通过校验的方式来保证数据一致，其方式也是如$UDP$校验一样增加伪首部与校验和。

#### 序号

$TCP$报文传输时每个字节都会编上序号，一个字节占用一个序号，并按报文段的形式一起发送，报文段长度不定，根据$MTU$来定。

序号字段指一个报文段第一个字节的序号。

序号建立在传送的字节流上，而不是报文段。

虽然$TCP$面向字节，但是不是每个字节都要发回确认，而是在发送一个报文段后才发回一个确认，确认号为报文段第一个字节的序号，所以$TCP$是对**报文段**的确认机制。

#### 确认

确认号是期望收到的下一个报文段数据的第一个字节的序号。

$TCP$缓存中的字节流按序传输后不会立刻在缓存中清除，而会等待接收方的确认字段，可以是单独确认也可以携带确认。

一般采用的是累计确认的方式。收到确认字段后就可以从缓存中清除对应报文段。

#### 重传

一般有两种情况会产生重传：

超时：

+ $TCP$每发送一个报文段就会设置一次计时器，$TCP$在重传时间内未收到确认就需要重传已发送的报文段。
+ 由于$TCP$下层互联网环境复杂，每次路由选择可能变化从而带来时延方差也很大，所以采用自适应算法：
  + 记录报文段发出时间和收到响应确认时间，称其差为报文段的往返时间$RTT$。
  + 根据$RTT$的测量值动态改变重传时间$RTT_s$（加权平均往返时间）。
  + 从而超时计时器设计的超时重传时间$RTO$应该略大于$RTT_S$。

新估计$RTT=(1-\alpha)\times$旧$RTT+\alpha\times$新$RTT$样本。

冗余$ACK$：

+ 为了加快发现需要重传的报文段，可以采用冗余$ACK$（冗余确认/快重传），每当比期望序号大的失序报文段到达时，发送一个冗余$ACK$，指明下一个期待字节的序号。
+ 如发送方已发送$1$，$2$，$3$，$4$，$5$报文段，接收方收到$1$会返回对$1$的确认（确认号为$2$的第一个字节），如果接收方收到$3$、$4$，都会返回对$1$的确认，发送方收到$3$个对于报文段$1$的冗余$ACK$就会认为$2$报文段丢失，从而快速重传$2$报文段。

### TCP协议流量控制

$TCP$使用滑动窗口机制来完成流量控制，与数据链路层的滑动窗口类似。单位为字节。

在通信过程中，接收方会根据接收缓存的大小，动态调整发送方发送窗口的大小，即接收窗口$rwnd$（接受方设置确认报文段的窗口字段，将$rwnd$通知给发送方），发送方的发送窗口取接收窗口$rwnd$和拥塞窗口$cwnd$（根据当前网络拥塞程度而由发送发确定的窗口值，与网络带宽与时延相关）的最小值。

$A$向$B$发送数据，连接建立时，$B$告诉$A$：$B$的$rwnd=400B$，设每一个报文段$100B$，报文段序号初始值为$1$。

![TCP流量控制][TCPcurrent]

$B$只有处理完接收窗口中的数据才能继续接收$A$的数据，发送$A$一个$rwnd$不为$0$的报文。

而如果这个告诉$A$接收窗口$rwnd$不为$0$的报文丢失了，$A$就一直会等待发送，$B$就会一直等待接收，从而产生死锁般的情况。

$TCP$为每一个连接设有一个持续计时器，只要$TCP$连接的一方收到对方的零窗口通知（即$rwnd=0$的通知）就启动持续计时器。

如果计时器设置的时间到期，$A$就会发送一个零窗口探测报文段，接收方收到探测报文段就会给出现在的窗口值。

如果窗口仍然是$0$，那么发送方就重新设置持续计时器。

### TCP协议拥塞控制

出现拥塞条件：需求>可用资源。

当网络中有许多资源同时呈现供应不足时网络性能变坏，网络吞吐量将随输入负荷增大而下降。

拥塞控制：防止过多的数据注入网络中。与流量控制不同的是它是面向全局的，是因为网络堵塞。形象来说拥塞控制就是为了控制路上堵车，而流量控制就是降低发车率。单位为$MSS$。

+ 接收窗口$rwnd$指接收方能接收缓存设置的值，并告知给发送方，反映接收方容量。
+ 拥塞窗口$cwnd$指发送方根据自己估算的网络拥塞程度而设置的窗口值，反映网络当前容量。

拥塞控制的假定：

1. 数据单方向发送，而另一个方向只发送确认，而不会捎带确认。
2. 接收方总是有足够大的缓存空间，因而发送窗口大小取决于拥塞程度。发送窗口=$\min$(接收窗口$rwnd$,拥塞窗口$cwnd$)。

#### 慢开始与拥塞避免

![慢开始与拥塞避免][congestavoid]

$cwnd$初始值是$1$，指一个最大报文段长度$MSS$。

传输轮次指发送了一批报文段并收到其确认的时间，一般指一个往返时延$RTT$。可能一次性传输多个报文。

1. 最开始是慢开始算法，一步步试探网络拥塞，开始时以$2$的指数形式增长。
2. $ssthresh$的意思是慢开始门限，代表从这个地方注入的报文段就比较多了，需要开始慢速增加了。
3. 拥塞窗口超过慢开始门限后进行拥塞避免算法，之后一段都是线性增长，每次增加$1$，直至达到网络拥塞状态。
4. 当网络开始拥塞时，进行乘法减小，瞬间将$cwnd$设置为$1$，同时调整原来的$ssthresh$的值到之前达到网络拥塞状态前值的$1/2$，（这里是$24$降到$12$），但是不能小于$2$。这样就能让拥塞的路由器能快速把队列中积压的分组处理完。
5. 重复以上步骤，但是注意此时$ssthresh$变了之后线性增长的转折点也变了。所以最后拥塞窗口会波动逼近适合当前网络拥塞状态的窗口值。

<span style="color:orange">注意：</span>当慢开始进行指数增长时，当$2cwnd>ssthresh$时，则一个$RTT$后$cwnd=ssthresh$，不会让慢开始的拥塞窗口超过阈值。

#### 快重传与快恢复

快重传（冗余$ACK$）在$TCP$协议可靠传输中已经提到过。

![快重传与快恢复][quickrepeat]

这里和上面的慢开始和拥塞避免的一开始步骤差不多，都是先指数增长再转变为线性增长。

不同的点是快重传和快恢复算法是在收到连续的$ACK$确认之后执行，这里的$ACK$就是冗余$ACK$，冗余$ACK$的特点是如果多次对某一段请求的数据没有被收到，达到一定数目，一般为三个冗余$ACK$之后就会立即执行重传。

但是此时只是降到现在$cwnd$的一半，再重新线性增长。而不是像慢开始和拥塞避免的从头开始，这就是快恢复。

一般而言 $TCP$ 建立连接和网络超时时使用慢开始和拥塞避免算法；当发送方接收到冗余 $ACK$ 时使用快重传和快恢复算法。
