## 1. 程序的内存模型

### 1.1 内存四区

1.  代码区：存放函数的二进制代码，由操作系统进行管理
2.  全局区：存放全局变量、静态变量、常亮
3.  栈区：由编译器自动分配释放，存储函数的参数值、局部变量等
4.  堆区：由程序员分配和释放、如果程序员不释放、程序结束时由操作系统释放

### 1.2 程序运行前

在程序编译后、运行该程序前，分为两个区域

1.  代码区：存放CPU执行的机器指令。
    1.  代码区是共享的在频繁的执行程序时只需要在内存中有一份代码即可。
    2.  代码区是只读的，防止程序被修改
2.  全局区
    1.  全局变量和静态变量存放于此
    2.  全局区还包括常量区，字符串常量和其他常量也存放于此
    3.  该区域的数据在程序结束后由操作系统释放
    4.  const修饰的全局变量（全局常量）也在全局区

### 1.3 程序运行后

1.  栈区：
    1.  由编译器自动分配释放，存放函数的参数值(形参)、局部变量
    2.  栈区的内存在函数运行结束后自动释放
    3.  注意：不要返回局部变量的地址，栈区开辟的数据由编译器自动释放
2.  堆区
    1.  由程序员分配释放，若程序员不释放，程序结束后由操作系统回收
    2.  在C++中主要用new来在堆区开辟内存
    3.  利用new开辟的内存空间是在堆区，保存这个内存地址的指针是在栈区（指针也有地址）
3.  new操作符
    1.  C++中利用new操作符在堆区开辟数据
    2.  堆区开辟的数据由程序员手动开辟，手动释放，释放利用操作符delete
    3.  语法：`new 数据类型`
    4.  利用new创建的数据，会返回该数据对应的类型的指针
    5.  利用new创建数组：`int *arr = new int[10]` 销毁时：`delete[] arr`

## 2. C++中的引用

>  引用是变量的别名，一旦把引用初始化为某个变量，就可以用该引用名称来指向变量
> 语法: `数据类型 &别名 = 变量；`

### 2.1 引用的注意事项

1. 不存在空引用，引用必须链接到一块合法的内存
2. 引用必须在创建时初始化
3. 一旦引用被初始化为某个变量，就不能再指向另一个变量
4. 引用不是对象，但仍要创建指针大小的空间 

```C++
int x = 10;
int &ref = x;	//引用的数据类型要和被引用的对象保持一致
```

### 2.2 函数传值的三种方式

>  传递的三种方式：值传递、地址传递、引用传递 

1. 值传递对形参的修改不会影响实参
2. 引用传递对形参的修改会影响实参,通过引用传递参数和地址传递的效果是相同的，引用更加简单
3. 地址传递传过去的地址，修改也是针对地址的   

### 2.3 引用做函数参数

1. 函数传递参数时，采用引用的形式让形参修饰实参，这样的好处是不会有值复制的过程（引用会创建一个指针大小的空间，**但引用不是对象**），和指针一样引用可以直接操作实参但是不会像指针那样复杂

### 2.4 引用做函数的返回值

1. ==注意：不要返回局部变量的引用==
2. 函数调用可以作为左值

```c++
int& func(){
    static int x = 10;
    return x;
}

func() = 121;
```

### 2.5 引用的本质是指针常量

编译器会自动将引用转换为指针常量,这也是为什么引用一旦创建就不能修改

```c++
int x = 10;
int &ref = x; //被转换为：int* const ref = &x;
ref = 100; 	//转换为：*ref = 100;
```

### 2.6 常量引用

1. 如果函数不会修改形参的值，那么形参就要使用常量引用防止误操作。
2. 对常量的引用必须使用常量引用，
3. 常量引用的本质为常量指针常量：`const int *const ref;`

```c++
const int &ref = 10;
// 10是常量，位于全局区，而引用的本质是指针，在栈区。
// 引用必须链接到一块合法的内存，所以 int &ref = 10 是不对的
// 而 const int &ref = 10 会被编译器转换为
int temp = 10;
const int &ref = temp;
```

## 3. 函数提高

### 3.1 函数的默认参数

在C++中，函数的形参列表中的形参是可以有默认值的

```C++
int func(int a, int b = 10, int c = 123){
    return a + b + c;
}
```

- 如果形参列表的某个形参有了默认值，那么从这个参数往后的所有参数都要有默认值
- 如果函数声明有了默认参数，那么函数定义就不能有默认参数，反之亦然

### 3.2 函数的占位参数

函数的形参列表里可以有占位参数，用来做占位，调用函数时必须填补该位置

```C++
void func(int a, int){
	cout << "this is func" << endl;
}

func(10,10);	//函数调用的时候需要给占位参数一个值，
```

- 主调函数传给占位参数的值并不能使用，主要用于函数重载
- 占位参数也可以有默认参数

## 3.3 函数重载

**函数名相同参数不同 **

- 同一个作用域下
- 函数名称相同
- 形参列表类型不同，个数不同，顺序不同
- 函数的返回值不能作为函数重载的条件

```c++
void func(int x, int y);
void func(int x, char c);
void func(int x);
//这三个函数都可以在同一个程序中被调用，称为重载
```

- 注意事项

  - 引用作为函数重载的条件时，变量会默认传递给非常量引用。常量只能传递给常量引用

  - 函数有无默认参数不会作为重载的条件

    ```c++
    void func(int x, int y = 10);
    void func(int x);
    func(12);	//此时函数调用会报错
    ```

    

# 4. 类和对象

**==面向对象三大特性：封装、继承、多态==**

- 

## 4.1 封装

- 封装的意义在于将属性和行为作为一个整体，表现现实中的事物，属性就是对象，行为就是函数。

- 同时对属性和行为加以权限控制，使得外部无法直接访问类中的成员，而是通过接口间接访问

类定义：

```c++
class classname{	
    Access specifiers:
    	Data members/variables;
    	
    	Member functions(){};
};
classname object1;
```

- 用`class`关键字来声明一个类，后跟类的名称，类定义包含在一对花括号中，以分号结尾
- 用访问修饰符来确定类成员的属性，有三种控制权限：public, private, protected

- 用类名来实例化对象

 

### 4.1.1 成员函数

- **成员函数**是指把函数的定义和声明写在类定义内部的的函数，类成员函数是类的一个成员，可以访问类的任意对象

- 成员函数可以定义在内部，也可以使用范围解析运算符`::`在外部定义定义，在类定义中定义的函数把函数定义为内联函数，即使没有使用`inline`标识符。

  ```C++
  calss A{
      public:
      	void func1(int x, int y){
              cout << "define in class" << endl;	//定义在类定义内部的函数，默认是内联函数
          }
      	void func2(int x, int y);
      	void func3(int x, int y);
  };
  
  inline void A::func2(int x, int y){
      cout << "define out class" << endl;
      //定义在类定义的外部的成员函数如果要声明为内联函数需要加上关键字inline
  }
  ```

  

### 4.1.2  类访问修饰符

数据封装是面向对象编程的一个重要特点，它防止函数直接访问类类型的内部成员。类成员的访问限制是通过在类主体内部对各个区域标记 **public、private、protected** 来指定的。关键字 **public、private、protected** 称为访问修饰符

- **public(公有)成员**：公有成员在类的外部是可访问的

  ```c++
  class A
  {
      public:
      	int x;
      	int y;
      	void func(int z);
  };
  A a1;
  a1.x = 123;	//可以直接访问类对象中的成员
  ```

  

- **private(私有)成员**：私有成员变量或函数在类的外部是不可访问的，甚至是不可查看的。只有类和友元函数可以访问私有成员。默认情况下，类的所有成员都是私有的。**==实际操作中，我们一般会在私有区域定义数据，在公有区域定义相关的函数，以便在类的外部也可以调用这些函数==**

  ```c++
  class B
  {
      private:
      	int x;
      public:
      	void func(int y){
              x = y;
          }
  };
  B b1;
  b1.x = 10;	//这个操作是非法的，正确操作如下所示
  b1.func(10);
  ```

- **protected(受保护)成员**：受保护成员变量或函数与私有成员十分相似，但有一点不同，受保护成员在派生类（即子类）中是可访问的。

  ```c++
  class C
  {
      private:
      	int x;
      protected:
      	int y;
  };
  class Cat::C	//Cat是派生类
  {
  	public:
      	int func()
          {
              return y;	//直接访问了基类中的受保护成员
          }
  };
  ```
  
  

## 4.2 对象的初始化和清理

### 4.2.1 构造函数和析构函数

类的**构造函数**是类的一种特殊的成员函数，它会在每次创建类的新对象时执行。构造函数的名称与类的名称是完全相同的，并且不会返回任何类型，也不会返回 void。构造函数可用于为某些成员变量设置初始值。

**构造函数和析构函数是程序的默认实现,如果不写,编译器也会默认创建一个空实现**

```c++
class D
{
	public:
    	D(int x){
            y = x;
            cout << "this is constructor func" << endl;
        }
    	~D(){
            cout << "D`s discontructor" < endl;
        }
    private:
    	int y;
};
D d1(2);	//构造函数在创建对象时会被调用，可以通过构造函数来个类的成员变量赋初值
```

**构造函数**

- 构造函数没有返回值，也不写void
- 函数名称和类名相同
- 构造函数可以有参数，因此可以发生重载
- 程序在创建对象时会自动调用构造函数，而且只会调用一次

**析构函数**

- 析构函数没有返回值也不写void
- 函数名称和类型相同，在名称前加上～
- 析构函数不可以有参数，因此不能发生重载
- 程序会在对象销毁前自动调用析构函数

**构造函数的调用**

- 括号调用

```C++
A a1;	//调用无参构造
A a2(10);	//有参构造
A a3(a1);	//拷贝构造
```

- 显示法

```C++
A a1;	//无参构造
A a2 = A(10);	//有参构造
A a3 = A(a1);	//拷贝构造
//形如: A(10), A(a1),的为匿名对象,在当前行结束后,系统会立即回收掉匿名对象
//不要用拷贝构造函数初始化匿名对象,
A(a1); 		//错误! 编译器会转换为 A a1;
```

- 隐式转换法

```C++
A a4 = 10; 	//等价于 A a4 = A(10);	有参构造
A a5 = a1; 	//等价于 A a5 = A(a1);	拷贝构造
```



### 4.2.2  拷贝构造函数

- 利用其他已经创建的对象的值来作为构造函数的参数

```c++
class A {
public:
    A(const A &a){
        x = a.x;
    }
private:
    int x;
};
A a1;
a1.SetX(10);
A a2(a1);
```

- **拷贝构造函数的调用分析**

  - 使用一个已经创建完毕的对象来**初始化**一个新的对象
  - 值传递的形式给函数参数传值
  - 以值方式返回局部对象（==注意：新版编译器优化会使得在返回对象时不会新创建内存空间，而是直接复制局部对象的地址，且在程序结束后不销毁局部对象==）

  ```C++
  class A{
  	public:
  		A(){
  			cout << "无参构造" << endl;
  		}
  		A(const A &a){
  			m_x = a.m_x;
  			cout << "copy constructor" << endl;
  		}
  		~A(){
  			cout << "disconstructor" << endl;
  		}
  
  	private:
  		int m_x;
  };
  
  void func1(A a){}	
  
  A func2(){
  	A a2;
  	return a2;
  }
  
  int main(){
  	A a1;
  	func1(a1);	//以值传递的形式传递参数本质上为： A a = a1;
  	A a3 = func2();	//返回值的本质如下所示,本质也是：A a3 = a2;
      //以上两种形式都是拷贝构造函数的隐式转换法
  	return 0;
  }
  ```

  

### 4.2.3 ==函数返回值的过程分析==

```C++
int func(){
	int x = 0;
    return x;
}
```

- 在函数调用过程中,有主调函数和被调函数之分, 一般而言,主调函数都是main函数,上面的func相当于被调函数
- 一般来讲, 返回值是返回给主调函数的，返回值类型为void时不会返回值
- 当被调函数执行到return语句时，编译器会开辟一片相应的内存空间存储返回值的数据，这片内存空间是匿名的且不会随着被调函数的结束而销毁。
- 当返回类型为引用时，编译器不会为返回值开辟内存空间，返回的是变量的别名，函数结束时内存销毁，所以不要返回局部变量的引用。



### 4.2.4 构造函数的调用规则

默认情况下，C++编译器至少给一个类添加三个函数

1. 默认构造函数	(空实现)
2. 默认析构函数    (空实现)
3. 默认拷贝构造函数    (值拷贝)

​	

**构造函数的调用规则为：**

1. 如果用户定义了无参构造,编译器会提供拷贝构造.

1. 如果用户定义了有参构造函数, C++编译器不再提供默认无参构造函数, 但会提供默认拷贝构造函数
2. 如果用户定义了拷贝构造函数,C++不再提供其他构造函数 
3.  不能只定义一个拷贝构造函数, 需要重载一个有参构造或者无参构造,使得程序能够正常运行,==拷贝构造函数不能单独存在==
4. 构造函数的等级: 拷贝构造函数 < 有参构造 < 无参构造, 如果有了拷贝, 后两个不需要,如果有了有参,无参就不需要, 

### 4.2.5 深拷贝与浅拷贝

```c++
class A{
private:
    A(int x. int y){
        m_x = x;
        m_y = new int(y);
        cout << "constructer" << endl;
    }
    //系统默认生成的拷贝构造函数
    A(const A &a){
    	m_x = a.m_x;
    	m_y = a->m_y;	//浅拷贝,复制传递过来的参数的地址
	}
    A(const A &a){
        m_x = a.m_x;
        m_y = new int(a.m_y);	//深拷贝,单独开辟一片内存空间
    }
    ~A(){
		if(m_y != NULL){
            delete m_y;
            m_y = NULL;
        }
    }
private:
    int m_x;
    int *m_y;
}:

int main(){
    A a1;
   	A a2(a1);	//这里调用系统默认生成的拷贝构造函数,是浅拷贝
    //深拷贝需要单独指定拷贝构造函数
	return 0;
}
```

- 浅拷贝就是在直接复制传递给拷贝构造函数的对象的成员的地址, 在析构函数中这个地址被释放了两次,为避免这种情况,需要用到深拷贝, 由用户指定拷贝构造函数,单独在堆区开辟内存空间
- 如果成员变量有在堆区开辟的,一定要自己提供拷贝构造函数,防止浅拷贝带来的问题(多次释放)

### 4.2.6 初始化列表

C++提供初始化列表语法,用来初始化成员变量

**语法**:`constructor():variable1(value1),variable2(value2),....{}`

```C++
class A{
    public:
    	A(int a, int b, int c):m_x(a),m_y(b),m_z(c){}
    	int m_x,m_y,m_z;
};
```

- 普通的初始化其实有一个值复制的过程, 比如`int x = 10;`, 编译器会先在全局区开辟一片空间把10存放在全局区, 然后在栈区开辟一片空间存放变量x, 最后把全局区的10复制给变量x
- 初始化列表则是直接把10存入位于栈区的变量x中



### 4.2.7 类对象作为类成员

```C++
class A{
public:
	A(int x){
		a_name = x;
		cout << "A`s constructor" << endl;
	}
	A(const A &a){
		a_name = a.a_name;
		cout << "A`s copy constructor" << endl;
	}
	~A(){
		cout << "A`s disconstructor" << endl;
	}
	int a_name;
};

class B{
public:
	B(string s, A a):b_s(s),b_a(a){
		cout << "B`s constructor" << endl;
	}
	~B(){
		cout << "B`s disconstructor" << endl;
	}
	string b_s;
	A b_a;
};

int main(){

	A a(9);

	B b("章三",a);

	return 0;
}

//执行结果
A`s constructor
A`s copy constructor
A`s copy constructor
B`s constructor
A`s disconstructor
B`s disconstructor
A`s disconstructor
A`s disconstructor
```

- C++一个对象可以是另一个对象的成员
- 两个对象的构造和析构遵循先进先出;

### 4.2.8 静态成员

静态成员就是在成员变量和成员函数前加上关键字static,称为静态成员,静态成员不属于某个特定对象, 而是属于这个类

1. **静态成员变量**
   1. 所有对象共享一份数据
   2. 在编译阶段分配内存
   3. ==类内声明,类外初始化==
   4. ==静态变量存放在全局区==

```C++
class A{
    public:
    	static int m_x;		//类内声明
    private:
    	static int m_y;		//类外不可以访问私有的静态成员变量
};
int A::m_x = 100;	//类外初始化
int A::m_y = 100;
int main(){
	A a1;
    A a2;
    a2.m_x = 200;
    cout << a1.m_x; 	//此时a1对象中的m_x的值已经变为200
    cout << A::m_x;		//也可以通过类名进行访问
    cout << a1.m_y;		//类外访问不了私有静态成员
}
```

2. **静态成员函数**
   
   1. 所有对象共享一个函数
   2. 静态成员函数只能访问静态成员变量,**因为静态成员函数在全局区, 而普通成员变量在堆区, 无法访问**
   2. 静态成员函数也是有访问权限的
   
   ```C++
   class A{
       public:
       	static void func(){
               cout << "static function" << endl;
           }
   };
   
   A::func();	//可以直接调用静态成员函数而不用指定对象,
   ```
   

## 4.3 C++对象模型和this指针

### 4.3.1 成员函数的成员变量分开存储

```C++
class A{
    public:
    	int a;	//非静态成员变量,属于类的对象上
    	void func1(){}	//非静态成员函数, 不属于类的对象上
    	static int a;	//静态成员变量, 不属于类的对象上
    	static void func(){}	//静态成员函数, 不属于类的对象上
};
```

- 空对象的大小不为0, 是随机大小.
- 只有非静态成员变量属于类对象上，类中的成员函数, 静态成员变量在这个类的所有对象中只有一份



### 4.3.2 this指针

- **类中的成员函数, 静态成员变量在这个类的所有对象中只有一份,也就是多个同类型的对象会共用一块代码**	

- 函数使用this指针区分是哪个对象调用的自己, **this指针指向被调用的成员函数所属的对象**, 哪个对象调用了成员函数,this指针就指向谁
- this指针是隐含在每一个非静态成员函数内部的指针
- **this指针本质上是指针常量**

**this指针的作用**

- 解决成员变量和非静态成员函数形参重名的问题,
- 返回对象本身用`*this`

```C++
class A{
    public:
    	void func(int x){
            this->x = x;
        }
    private:
    	int x;
};
```

==空指针仍然可以访问成员函数,==但是空指针访问不了成员变量,原因也是因为成员函数是在类中的,而不是对象中

编译器在访问对象的成员变量时会默认加上this指针,将m_x转换为:this->m_x,因为this指针是空的,所以没有办法访问其成员变量

```C++
class A{
    public:
    	void func1(){
            cout << "member function" << endl;
        }
    	void func2(){
            cout << "m_x: " << m_x << endl;
            
        }
    	int m_x;
};

int main(){
	A *pa  = NULL;
    pa->func1();	//正确
    pa->func2():	//错误
}
```



### 4.3.3 const修饰成员函数和成员变量

```C++
class A{
    public:
    	void func(int x) const{
            m_x = x; 	//错误,常函数中不可以修改成员变量的值
            m_y = x;	//正确，mutable类型的变量在常函数中仍然可以被修改
        }
    	int m_x;
    	mutable int m_y;
};
int main(){
    const A a;	//常对象
}
```

- 在成员函数后面加上`const`表示,成员函数为**常函数**, 常函数中的所有变量都是只读的,不可以修改变量的值.
- 实际上const修饰的是this指针,上面的代码可写为`const this->m_x= x;`显然这是错误的
- 在成员变量前加上`mutable`关键字表示这个变量在常函数中仍然可修改
- 不能修改常对象中的值。`mutable`变量除外
- **常对象只能调用常函数**



## 4.4 友元

- 类的友元函数是定义在类外部，但有权访问类的所有私有（private）成员和保护（protected）成员。尽管友元函数的原型有在类的定义中出现过，但是友元函数并不是成员函数。
- 友元可以是一个函数，该函数被称为友元函数；友元也可以是一个类，该类被称为友元类，在这种情况下，整个类及其所有成员都是友元。如果要声明函数为一个类的友元，需要在类定义中该函数原型前使用关键字 **friend**
- **做友元的三种情况**
  - 全局函数做友元
  - 类做友元
  - 成员函数做友元


```C++
class Box{
    friend void PrintValue(Box &box);	//声明PrintValue函数为Box的友元函数
    public:
    	void SetValue(int weight){}
    	
    	friend class Circle;	//声明Circle类的所有成员函数作为Box的友元
    private:
    	int width;
    	int height;
    	int lenght
};
void PrintValue(Box &box){
    cout << width << endl;
}
int main(){
	Box box;
    PrintValue(box);
}
```

- 友元函数没有隐含的this指针， 所以要特别指定访问的是哪一个对象
- 友元函数不需要特别指定访问权限

```C++
class Building;
class GoodGay{
public:
	GoodGay();
    ～GoodGay();
	void visit();
	Building *building;
};
class Building{
	friend class GoodGay;	//类作友元
    friend vvoid GoodGay::visit();	//类中的成员函数作友元
public:
	Building();
	string m_settingRoom;
private:
	string m_BedRoom;
};
Building::Building(){
	m_settingRoom = "洗手间";
	m_BedRoom = "卧室";
}
GoodGay::GoodGay(){
	building = new Building;
}
~GoodGay(){
	delete building;
}
void GoodGay::visit(){
	cout << "GoodGay::visit m_settingRoom" << building->m_settingRoom << endl;
	cout << "GoodGay::visit m_BedRoom" << building->m_BedRoom << endl;
	//GoodGay类的成员访问Building类的私有成员，需要在Building类中将GoodGay类设置为友元类
}
int main(){
	GoodGay g;
	g.visit();
	return 0;
}
```





## 4.5 运算符重载

- C++ 允许在同一作用域中的某个**函数**和**运算符**指定多个定义，分别称为**函数重载**和**运算符重载**。
- 重载声明是指一个与之前已经在该作用域内声明过的函数或方法具有相同名称的声明，但是它们的参数列表和定义（实现）不相同。
- 当您调用一个**重载函数**或**重载运算符**时，编译器通过把您所使用的参数类型与定义中的参数类型进行比较，决定选用最合适的定义。选择最合适的重载函数或重载运算符的过程，称为**重载决策**。
- **运算符重载**的意思是:对已有的运算符进行重新定义, 赋予其另一种功能, 以适应不同的数据类型
- 重载的运算符是带有特殊名称的函数，函数名是由关键字` operator `和其后要重载的运算符符号构成的。与其他函数一样，重载运算符有一个返回类型和一个参数列表。
- 既可以通过成员函数重载,也可以通过全局函数重载
- **运算符重载时也可以发生函数重载**
- 内置数据类型不可以发生重载
- 不要滥用运算符重载

### 4.5.1 加号运算符重载

```C++
class Person{
public:
	Person(int x = 0, int y = 0){
		m_x = x;
		m_y = y;
	}
	void Print(){
		cout << "m_x: " << m_x << endl;
		cout << "m_y: " << m_y << endl;
	}
    //成员函数重载
    Person operator+(Person &p){
        Person temp;
        this->m_x = p.m_x;
        this->m_x = p.m_y;
        return temp;
    }
	int m_x;
	int m_y;
};

Person operator+(const Person &p1, const Person &p2){
	Person p3;
	p3.m_x = p1.m_x + p2.m_x;
	p3.m_y = p1.m_y + p2.m_y;
	return p3;
}


int main(){

	Person p1(10,20);
	Person p2(10,20);
    //全局函数
    Person p3 = operator+(p1,p2);	
	//简化为:
   	Person p3 = p1 + p2;
    //成员函数重载
    Person p3 = p1.operator+(p2);
    //简化为
    Person p3 = p1 + p2;
	p3.Print();
	return 0;
}
```

### 4.5.2 左移运算符重载

**用于输出自定义的数据类型**

```C++
class Person{
	friend ostream& operator<<(ostream &cout, Person &p);
public:	
	Person(int x, int y):m_x(x),m_y(y){};
private:
	int m_x;
	int m_y;
};
ostream& operator<<(ostream &cout, Person &p){
	cout << "m_x: " << p.m_x << "m_y: " << p.m_y << endl;
	return cout;
}
int main(){

	Person p(10,20);

	cout << p << endl;

	return 0;
}
```

- 左移运算符重载不能用成员函数,只能用全局函数, (因为左移运算符的左边必须是cout,如果是成员函数cout只能只能放在右边,形如:`p.operator<<(cout)`)
- cout的类类型为ostream, 流对象不能复制, 只能引用
- 返回值类型为ostream是为了链式调用
- 声明为友元函数是为了访问成员的私有成员
- **重载的左移运算符配合友元可以实现输出自定义数据类型**

### 4.5.3 重载递增运算符

- 前置递增运算符是直接递增变量,	而后置要先递增拷贝旧值返回

```C++
class Box{
	friend ostream& operator<<(ostream& cout, const Box& b);
public:
	Box(int x = 0, int y = 0):m_x(x),m_y(y){};
	Box& operator++(){
		m_x++; m_y++;
		return *this;
	}
	Box operator++(int){
		Box temp;
		m_x++; m_y++;
		return temp;
	}
private:
	int m_x, m_y;
};

ostream& operator<<(ostream& cout, const Box& b){
	cout << "m_x: " << b.m_x << endl;
	cout << "m_y: " << b.m_y << endl;
	return cout;
}

int main(){

	Box b1;

	cout << ++b1 << endl;
	b1++;
	cout << b1 << endl;

	return 0;
}
```

### 4.5.4 ==前置递增和后置递增的区别==

前置++和后置++，有4点不同：返回类型不同、形参不同、代码不同、效率不同

1. 返回值类型不同:

   - 前置++的返回类型是左值引用，后置++的返回类型const右值。而左值和右值，决定了前置++和后置++的用法。

   - ++a的返回类型为什么是引用呢？
     - 这样做的原因应该就是：与内置类型的行为保持一致。前置++返回的总是被自增的对象本身。因此，++(++a)的效果就是a被自增两次。

2. 形参的区别

   前置++没有形参，而后置++有一个int形参，但是该形参也没有被用到。很奇怪，难道有什么特殊的用意？

   其实也没有特殊的用意，只是为了绕过语法的限制。

   前置++与后置++的操作符重载函数，函数原型必须不同。否则就违反了“重载函数必须拥有不同的函数原型”的语法规定。

   虽然前置++与后置++的返回类型不同，但是返回类型不属于函数原型。为了绕过语法限制，只好给后置++增加了一个int形参。

   原因就是这么简单，真的没其他特殊用意。其实，给前置++增加形参也可以；增加一个double形参而不是int形参，也可以。只是，当时就这么决定了。

3. 代码实现的区别

   前置++的实现比较简单，自增之后，将*this返回即可。需要注意的是，一定要返回*this。

   后置++的实现稍微麻烦一些。因为要返回自增之前的对象，所以先将对象拷贝一份，再进行自增，最后返回那个拷贝。注意一定要是拷贝, 因为不能返回局部变量的引用

4. 效率的区别

   如果不需要返回自增之前的值，那么前置++和后置++的计算效果都一样。但是，我们仍然应该优先使用前置++，尤其是对于用户自定义类型的自增操作。

   前置++的效率更高，理由是：后置++会生成临时对象。

5. **前置版本将对象本身作为左值返回，后置版本则将原始对象的副本作为右值返回，两种运算符必须作用于左值运算对象。后置版本需要拷贝副本，所以会影响程序的性能**, C++Primer（P132）



### 4.5.5 赋值运算符重载

**C++编译器至少给一个类添加四个函数**

- 默认构造函数(无参, 函数体为空)
- 默认析构函数(无参, 函数体为空)
- 默认拷贝构造函数, 对属性进行值拷贝(**拷贝构造函数只会在对象初始化时才会调用, 如果要在其他时间给对象赋值, 就需要用到赋值运算符**)
- 赋值运算符operator=, 对属性进行值拷贝



系统默认的赋值赋值运算符会有浅拷贝的问题,所以需要重载赋值运算符=



```C++
class Box{
public:
	Box(int *x):m_x(x){};
	Box& operator=(Box &b){	//要谨慎考虑对象的复制，因为这会调用拷贝构造函数
		if(m_x != NULL){	//防止内存泄漏，重新指向新的内存空间之后，原本的内存空间就无法释放了
			delete m_x;
			m_x = NULL;
		}
		m_x = new int(*b.m_x);
		return *this;
	}
	~Box(){
		if(m_x != NULL){
			delete m_x;
			m_x = NULL;
		}
	}
	int *m_x;

};

int main(){
	int *x1 = new int(3);
	int *x2 = new int(4);
	Box b1(x1);
	Box b2(x2);
	b2 = b1;
	cout << "b2.m_x: " << *b2.m_x << endl;
	return 0;
}
```





### 4.5.6 关系运算符重载

```c++
class Box{
public:
	Box(int age, string name):m_age(age),m_name(name){};
	bool operator==(Box &b){
		if(m_name == b.m_name && m_age == b.m_age)
			return true;
		return false;
	}
	int m_age;
	string m_name;
};


int main(){

	Box b1(18,"张三");
	Box b2(18,"李四");
	if(b1 == b2)
		cout << "b1和b2是相等的" << endl;
	else
		cout << "b1和b2是不等的" << endl;

	return 0;
}
```

不相等、大于、小于类似

### 4.5.7 函数调用运算符重载

函数名后面的一对小括号`()`就是就是函数调用运算符

重载了小括号, 看起来像是函数, 因此也叫**仿函数**

```C++
class Box{
public:
	void operator()(string word){
		cout << word << endl;
	}
    void operator()(int x){
		cout << x << endl;
	}
};

int main(){
	Box b1;
	b1("hello world");	//b1是对象名, 不是函数名
    b1(2);
	return 0;
}
```

## 4.6 继承

**继承是面向对象三大特性之一**, 继承允许我们利用一个已经存在的类创建一个新类， 已有的类称为**基类**，新建的类称为**派生类**，**派生类包含基类的所有非静态成员**，并在此基础上有所拓展，减少代码量。

### 4.6.1 基类和派生类

一个类可以派生自多个类，这意味着，它可以从多个基类继承数据和函数。定义一个派生类，我们使用一个类派生列表来指定基类。类派生列表以一个或多个基类命名，形式如下：

`class derived-class: access-specifier base-class`

其中，访问修饰符 access-specifier 是 **public、protected** 或 **private** 其中的一个，base-class 是之前定义过的某个类的名称。如果未使用访问修饰符 access-specifier，则默认为 private。

### 4.6.2 访问控制和继承

派生类可以访问基类中所有的非私有成员。

| 访问     | public | protected | private |
| -------- | ------ | --------- | ------- |
| 同一个类 | yes    | yes       | yes     |
| 派生类   | yes    | yes       | no      |
| 外部的类 | yes    | no        | no      |

一个派生类继承了所有的基类方法，但下列情况除外：

- 基类的构造函数、析构函数和拷贝构造函数。
- 基类的重载运算符。
- 基类的友元函数。

---

### 4.6.3 继承类型

当一个类派生自基类，该基类可以被继承为 **public、protected** 或 **private** 几种类型。继承类型是通过上面讲解的访问修饰符 access-specifier 来指定的。

我们几乎不使用 **protected** 或 **private** 继承，通常使用 **public** 继承。当使用不同类型的继承时，遵循以下几个规则：

- **公有继承（public）：**当一个类派生自**公有**基类时，基类的**公有**成员也是派生类的**公有**成员，基类的**保护**成员也是派生类的**保护**成员，基类的**私有**成员虽然也被继承了不能直接被派生类访问，但是可以通过调用基类的**公有**和**保护**成员来访问。
- **保护继承（protected）：** 当一个类派生自**保护**基类时，基类的**公有**和**保护**成员将成为派生类的**保护**成员。
- **私有继承（private）：**当一个类派生自**私有**基类时，基类的**公有**和**保护**成员将成为派生类的**私有**成员。

### 4.6.4 多继承

多继承即一个子类可以有多个父类，它继承了多个父类的特性。

```C++
class <派生类名>:<继承方式1><基类名1>,<继承方式2><基类名2>,…
{
<派生类类体>
};
```

- 不建议使用多继承，因为基类可能来自不同的人之手，一旦某一个基类被修改，继承其的派生类也要被修改

### 4.6.5 继承中的构造和析构顺序

- 派生类会继承基类的所有成员，包括构造析构函数，因此在创建派生类对象时也会调用基类的构造析构函数

- 具体顺序为：先调用基类的构造函数然后是派生类，析构函数遵循先进先出原则；
- 在多继承中，按继承顺序来调用构造函数，同样遵循先进先出原则

### 4.6.6 继承中的同名成员处理

```C++
class Base1{
public:
	Base1(int b_x){
		m_x = 1;
		cout << "base1 constructor, b_x: " << b_x << endl;
	}
	~Base1(){
		cout << "base1 disconstructor" << endl;
	}
	int m_x;
};
class Base2{
public:
	Base2(){
		cout << "base2 constructor" << endl;
	}
	~Base2(){
		cout << "base2 disconstructor" << endl;
	}
};
class derived_base : public Base2, public Base1{

public:
	derived_base(int d_x): Base1(d_x){
		m_x = 2;
		cout << "derived constructor" << endl;
	}
	~derived_base(){
		cout << "derived disconstructor" << endl;
	}
	int m_x;

};

int main(){

	derived_base dd(2);
	cout << "dd.m_x: " << dd.m_x << endl;
	cout << "dd.Base1::m_x: " << dd.Base1::m_x << endl;

	return 0;
}
```

- 当基类和派生类出现同名成员时， 访问基类成员需要加上作用域解析运算符
- 派生类会隐藏基类的同名成员， 如果需要访问被隐藏的基类成员需要加作用域

### 4.6.7 菱形继承

基类被派生类1和派生类2继承，派生类3又继承了派生类1和2， 当调用派生类3时就会重复调用基类，导致二义性

```C++
class Base{
public:
	Base(){
		m_x = 10;
	}
	int m_x;
};

class Derived1: public Base{
public:
	Derived1(){
		d1_x = 1;
	}
	int d1_x;
};

class Derived2: public Base{
public:
	Derived2(){
		d2_x = 2;
	}
	int d2_x;
};

class Derived3 : public Derived1, public Derived2{
public:
	Derived3(){
		d3_x = 3;
	}
	int d3_x;
}; 

int main(){

	Derived3 d3;
	cout << d3.Derived1::m_x << endl;

	return 0;
}
```

- 在菱形继承中，两个基类拥有相同的数据，访问成员时需要加上作用域
- 在上述代码中Base类被继承了两次，造成内存浪费
- 利用虚继承解决这一问题

```C++
class Base{
public:
	Base(){
		m_x = 10;
	}
	int m_x;
};

class Derived1: virtual public Base{
public:
	Derived1(){
		d1_x = 1;
	}
	int d1_x;
};

class Derived2: virtual public Base{
public:
	Derived2(){
		d2_x = 2;
	}
	int d2_x;
};

class Derived3 :  public Derived1, public Derived2{
public:
	Derived3(){
		d3_x = 3;
	}
	int d3_x;
}; 

int main(){

	Derived3 d3;
	cout << d3.m_x << endl; //此时这样访问，就不会报错了

	return 0;
}
```

- 虚继承的基类称为**虚基类**
- 虚继承的对象继承的不是基类的数据，而是一个指针`vbptr`，该指针通过偏移量找到唯一的数据
- `vbptr`指向虚基类表`vbtable`，虚基类表存储每个数据的偏移量
- 在派生类中访问虚基类的数据，其实就是通过指针加上偏移量指向基类数据

## 4.7 多态

> 多态是C++面向对象三大特性之一，这里的多态专指动态多态

多态分两类：静态多态和动态多态(编译时多态和运行时多态)

- 静态多态：函数重载和运算符重载，属于静态多态，
- 动态多态：**派生类**和**虚函数**实现运行时多态

**静态多态的动态多态的区别**

- 静态多态的函数地址早绑定 - 编译阶段确定函数地址
- 动态多态的函数地址晚绑定 - 运行时阶段确定函数地址

```C++
class Animal{
public:
	virtual void Speak(){
		cout << "animal is speak" << endl;
	}
};


class Cat: public Animal{
public:
	void Speak(){	//子类重写虚函数时，virtual可加可不加
		cout << "cat is speak" << endl;
	}
};

void doSpeak(Animal &animal){
	animal.Speak();
}

int main(){

	Cat cat;
	doSpeak(cat);

	return 0;
}
```

- 在c++中允许基类和派生类的对象相互传递
- 当基类的函数为虚函数，且在派生类中重写了基类的函数，然后将派生类传递给基类对象，才是多态

**动态多态的满足条件：**

- 有继承关系
- 派生类重写基类的虚函数

**动态多态的使用：**基类的指针或引用， 指向派生类的对象



### 4.7.1 虚函数
- **虚函数** 是在基类中使用关键字 **virtual** 声明的函数。在派生类中重新定义基类中定义的虚函数时，会告诉编译器不要静态链接到该函数。我们想要的是在程序中任意点可以根据所调用的对象类型来选择调用的函数，这种操作被称为**动态链接**，或**后期绑定**。
- 成员函数在类的所有对象中只有一份，成员函数使用this指针来分辨是哪个类的调用，this指针指向调用成员函数的对象，
- 类以及类的对象只存储了成员变量，没有存储成员函数，类的所有对象共享同一份成员函数
- 而虚函数在类中是作为指针（vfptr）存储的，该指针指向虚函数表vbtable，表内记录虚函数的地址(&Animal::speak)，当派生类重写基类的虚函数时，派生类中的虚函数指针vfptr会指向派生类的虚函数表，表中记录派生类的虚函数地址(&Cat::speak)
- 当基类的指针或引用指向派生类的对象时，基类的虚函数指针指向了派生类的虚函数表，表中为派生类的虚函数地址
- 多态的本质就是根据传入不同的子类对象，来指向对应的虚函数表
- 虚函数可以不实现，不实现的虚函数就是纯虚函数

### 4.7.2 纯虚函数和抽象类
纯虚函数声明如下： `virtual void funtion1 () = 0`
纯虚函数一定没有定义，纯虚函数用来规范派生类的行为，即接口。包含纯虚函数的类是抽象类，抽象类不能定义实例，但可以声明指向实现该抽象类的具体类的指针或引用。
```c++
class Animal {
public:
	// 虚函数在基类中必须有定义
	virtual void speak(){
		cout << "Animal speak" << endl;
		return;
	virtual void eat() = 0;
	//=0 告诉编译器，函数没有主体，上面的函数就是纯虚函数
	}
};

class Cat: public Animal{
public:
	void speak(){
		cout << "Cat speak" << endl;
		return;
	}
	void eat(){
	//纯虚函数在子类中的实现
		cout << "Cat eat" << endl;
		return;
	}
};
};

```

### 4.7.3 多态的表现形式
1. 多态的表现形式:指针
   - 派生类的地址可以赋给基类指针
   - 通过基类指针调用基类和派生类中的同名函数时，
     - 若指针指向基类的对象，那么调用的是基类的虚函数
     - 若指针指向派生类的对象，那么调用的是派生类的对象
   - 这种机制就是多态，实际上就是**调用那个虚函数取决于指针指向那个类型的对象**
```C++
class Father{
    public:
        virtual void func(){
            cout << "father" << endl;
            return;
        }
};

class Son1: public Father{
    public:
        virtual void func(){
            cout << "son1" << endl;
            return;
        }
};

class Son2:public Father{
    public:
        virtual void func(){
            cout << "son2" << endl;
            return;
        }
};

int main(){
	Son1 son1;
    Son2 son2;
    Father *father1;
	Father &father2 = son2;
    father1 = &son1;
    father1->func();
    father2.func();
    return 0;
}
```
2. 多态的表现形式2：引用
   - 派生类的对象可以赋给基类引用
   - 通过基类引用调用基类或者派生类的成员时
     - 如果若该基类引用引用的是一个基类对象，那么调用的是基类的虚函数
     - 如果该基类引用引用的是一个派生类对象，那么调用的是派生类的对象

### 4.7.4 多态的实现原理
>「多态」的关键在于通过**基类指针或引用**调用一个**虚函数**时，编译时不能确定到底调用的是基类还是派生类的函数，运行时才能确定。


```C++
class A 
{
public:
    int i;
    virtual void Print() { } // 虚函数
};

class B
{
public:
    int n;
    void Print() { } 
};

int main() 
{
    cout << sizeof(A) << ","<< sizeof(B);
    return 0;
}
```
- 有虚函数的类多出了 8 个字节，也就是一个指针的大小
- 每个有虚函数的类（或有虚函数基类的派生类） 都有一个虚函数表，该类的任何对象中都放着一个**虚函数表的指针**，虚函数表中存放该类所有虚函数的地址
- 当派生类**重写**基类的虚函数时，派生类中的虚函数指针 vfptr 会指向派生类的虚函数表，表中记录派生类的虚函数地址 (&Cat :: speak)
- 派生类中没有重写的虚函数，在该派生类的虚函数表中的地址和基类虚函数表的地址是相同的
- 多态的函数调用语句被编译成一系列根据基类指针所指向的（或基类引用所引用的）对象中**存放的虚函数表的地址**，在虚函数表中查找虚函数地址，并调用虚函数的指令。

### 4.7.5 虚析构函数

> 在多态的情况下，通过基类指针删除派生类的对象时通常只会调用基类的析构函数，派生类的析构函数没有被调用，
```C++
class A {
public:
	A() {
		cout << "A constructor" << endl;
	}
	~A(){
		cout << "A deconstructor" << endl;
	}
};


class B : public A{
public:
	B() {
		cout << "B constructor" << endl;
	}
	~B(){
		cout << "B deconstructor" << endl;
	}
};

int main(){

	A *pa = new B();
	delete pa;
	return 0;
}

```

**解决办法：把基类的析构函数声明为virtual**

-   派生类的析构函数可以 virtual 不进行声明；
-   通过基类的指针删除派生类对象时，首先调用派生类的析构函数，然后调用基类的析构函数，还是遵循「先构造，后虚构」的规则。


### 4.7.6 笔记
- 对于虚函数，基类和派生类都有各自的版本，由多态方式调用的时候动态绑定
- 实现了纯虚函数的子类，该纯虚函数在子类中就变成的虚函数，子类的子类就可以重写该虚函数，由多态方式调用的时候动态绑定
-   一个类如果定义了虚函数，则应该将析构函数也定义成虚函数;
-   或者，一个类打算作为基类使用，也应该将析构函数定义成虚函数。
-   注意：构造函数不能定义成虚构造函数。


# 4.7.7 面向对象特性
- **数据抽象**
	- 数据抽象是指，只向外界提供关键信息，并隐藏其后台的实现细节，即只表现必要的信息而不呈现细节。
	- 数据抽象是一种依赖于接口和实现分离的编程（设计）技术。
- **数据封装**
	- 封装是面向对象编程中的把数据和操作数据的函数绑定在一起的一个概念，这样能避免受到外界的干扰和误用，从而确保了安全。数据封装引申出了另一个重要的 OOP 概念，即**数据隐藏**。
	- **数据封装**是一种把数据和操作数据的函数捆绑在一起的机制，**数据抽象**是一种仅向用户暴露接口而把具体的实现细节隐藏起来的机制。
	- C++ 通过创建**类**来支持封装和数据隐藏（public、protected、private）。我们已经知道，类包含私有成员（private）、保护成员（protected）和公有成员（public）成员。默认情况下，在类中定义的所有项目都是私有的。
- **C++ 接口**
	- 接口描述了类的行为和功能，而不需要完成类的特定的实现
	- C++ 接口是使用**抽象类**来实现的，抽象类与数据抽象互不混淆，数据抽象是一个把实现细节与相关的数据分离开的概念。
	- 如果类中至少有一个函数被声明为纯虚函数，则这个类就是抽象类。纯虚函数是通过在声明中使用 "= 0" 来指定的
	- 设计**抽象类**（通常称为 ABC）的目的，是为了给其他类提供一个可以继承的适当的基类。抽象类不能被用于实例化对象，它只能作为**接口**使用。如果试图实例化一个抽象类的对象，会导致编译错误。
- **设计模式**
	- 面向对象的系统可能会使用一个抽象基类为所有的外部应用程序提供一个适当的、通用的、标准化的接口。然后，派生类通过继承抽象基类，就把所有类似的操作都继承下来。

	- 外部应用程序提供的功能（即公有函数）在抽象基类中是以纯虚函数的形式存在的。这些纯虚函数在相应的派生类中被实现。

	- 这个架构也使得新的应用程序可以很容易地被添加到系统中，即使是在系统被定义之后依然可以如此。

# 5. 文件操作
 - C++中读文件和写文件都要用到标准库 `fstream`, fstream 分为 `ofstream` 和 `ifstream`
**打开文件**
- 打开文件需要使用 ofstream 中的一个方法 `open ()`, open 函数是 fstream, ifstream, ofstream 对象的一个成员
- 具体定义为: `void open(const char *filename, ios::openmode mode)`
- open函数第一个成员参数的第一个成员指定要打开的文件和位置,第二个文件定义要打开的模式
- 
