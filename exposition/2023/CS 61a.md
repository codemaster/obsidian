## What is Computer Science

- The study is
	- What the problems can be solved using computation
	- How to solve this problems  and,
	- What techniques lead to use effecitive solutions
- 计算机是一个复杂系统（complexity），通过抽象来管理这个复杂系统
- Managing Complexity
	- Mastering Abstraction
	- Programming paradigms
- Introduce programming
	- Full understanding of Python fundamentals
	- Combining multiple ideas in large projects
	- How computer interpret programming languages
- 课程所使用的编程语言是：[[python]]
- 