**Tmux是一款终端复用工具**

 tmux是终端多路复用器。 它让您在一个终端中的多个程序之间轻松切换，分离它们（它们在后台运行）并将其重新连接到不同的终端。
![[tmux.png]]
## 1. 概念
**首先**明确三个概念，会话session、窗口window、窗格panel。
- 会话：会话表示用户和计算机之间的一个交流关系，一个会话可以建立多个窗口。会话有两个对立状态，attached(连接)和detached(分离)，连接就是用户和会话之间建立了正常连接，可以交互。当用户关闭终端attached状态就会变为detached状态，也可以手动转为detached状态；分离就是用户和会话之间断开了连接，需要先转为attached状态才能交互。
-   窗口：window就是当前终端的窗口，如果终端全屏也就是你一眼可以看到的整个屏幕。
-   窗格：一个窗口可以分成多个窗格，就是将当前屏幕分成好几块（分屏）。
## 2. 常用命令
1. 会话
		1. `tmux`：启动tmux,这会创建一个默认会话，编号是0,依次类推，`exit`退出会话
		2. `tmux new -S <session-name>`：创建一个指定名称的会话
		3. `tmux list-session/ls`：列出所有会话
		4. `tmux switch -t <session-name>/<session-num>：切换会话
		5. `tmux detach`：将当前会话与窗口（这里的窗口指的是terminal window）分离
		6. `tmux attach -t <session-name>/<session-num>`：接入会话
		7. `tmux kill-session -t <session-name>/<session-num>`：杀死会话
		8. `tmux kill-session -a -t <session-name>`：杀掉除了指定会话之外的所有会话，
		9. `tmux kill-session -a`杀掉除当前外的所有会话
		10. `tmux rename-session -t session-num <new-name>`：重命名会话
2. 窗口
		1. `tmux new-window -n <window-name>`：创建一个指定名称的窗口
		2. `tmux new -s <session-name> -n <window-name>`：创建会话时创建窗口，同时指定名称
		3. `tmux select-window -t <window-num>/<window-name>`:切换窗口
3. 窗格
		1. `tmux split-window`：划分上下两个窗格
		2. `tmux split-window -h`：划分左右两个窗格
## 3. 快捷键
1. 会话
		1. `ctrl+b s`：列出所有会话
		2. `ctrl+b $`：重命名会话
		3. `ctrl+b (`：移动到上一个会话
		4. `ctrl+b )`：移动到下一个会话
		5. `ctrl+b d`：将会话与当前窗口（这里的窗口指的是terminal window）分离
		6. `ctrl+d`：退出会话
2. 窗口
		1. `ctrl+b c`：创建一个窗口
		2. `ctrl+b ,`：重命名当前窗口
		3. `ctrl+b &`：关闭当前窗口
		4. `ctrl+b p/n`：移动到上一个/下一个窗口
		5. `ctrl+b num`：移动到指定窗口
3. 窗格
		1. `ctrl+b %`：竖分窗格
		2. `ctrl+b "`：横分窗格
		3. `ctrl+b <arrow-key>`：切换窗格
		4. `ctrl+b o`：切换到下一个窗格
		5. `ctrl+b q`：显示窗格编号
		6. `ctrl+b q num`：切换到指定窗格
		7. `ctrl+b x`：关闭当前窗格
		8. `ctrl+b !`：将当前窗格拆分成一个独立的窗口
		9. `Ctrl+b {`：当前窗格与上一个窗格交换位置。
		10. `Ctrl+b }`：当前窗格与下一个窗格交换位置。 
		11. `Ctrl+b z`：当前窗格全屏显示，再使用一次会变回原来大小。
## 4. 其他
1. 新建会话`tmux new -s my_session`。
2. 在 Tmux 窗口运行所需的程序。 
3. 按下快捷键`Ctrl+b d`将会话分离。
4. 下次使用时，重新连接到会话`tmux attach-session -t my_session`。
