# 1. gcc
## 1.1 编译过程
1. 预处理 Pre-Progressing
```md
# -E 选项指示编译器仅对输入文件进行预处理, 生成的是.i文件
g++ -E test.cpp
```
2. 编译 Compiling
```md
# -S 编译选项告诉编译器在为C++产生汇编语言之后停止编译，生成的是.s文件
g++ -S test.i
```
3. 汇编 Assembling
```md
# -c 选项告诉编译器仅把源代码编译为机器语言代码，生成的是.o文件
g++ -c test.s
```
4. 链接 Linking
```md
# -o选项指定编译之后的文件名
g++ test.o -o test
```

## 1.2 g++ 编译参数
1. `-g` : 编译带调试信息的可执行文件，只有这样才能使用gdb调试
2. `-O[n]` : 优化源代码，产生尽可能小和尽可能快的代码，n 取 1、2、3，表示优化等级
3. `-Wall`：打印警告信息,`-w`：关闭所有警告信息
4. `-std=c++11` ：设置编译标准， `g++ -std=c++11 test.cpp` 使用c++11标准编译
5. `-o`：指定生成的文件名
6. `-l`：用来指定程序要链接的库，-l参数紧跟着库名，编译器会在指定的`/usr/lib, /lib, /usr/local/lib`的这三个库中搜索
7. `-L`：指定库文件路径，如果库文件没有放在以上三个路径中，就要指定库文件所在路径，`g++ -L/home/zyanjun/lib -lgogs test.cpp`
8. `-I`：指定头文件搜索目录 `g++ -I/home/zyanjun/project/hello/include main.cpp`， 一般头文件目录不用指定gcc默认为`/usr/include`，但是自己写的头文件就需要指定路径了，可以用相对路径来指定`g++ -Iinclude main.cpp`
9. 生成库文件
	1. 生成静态库：
		1. 汇编生成机器语言代码，`g++ test.cpp -c  test.o`
		2. 生成静态库，`ar rs libtest.a test.o` ，静态库文件以`.a`结尾
		3. 编译，`g++ -Iinclude -Lsrc -ltest main.cpp -o staticmain`，指定库文件名为test，编译器就会到指定目录去搜索`libtest.a`文件
	2. 生成动态库：
		1. 汇编生成机器语言代码，`g++ -c test.cpp -fPIC`， `fPIC`告诉编译器产生与位置无关的代码
		2. 生成动态库，`g++ -shared test.o -o libtest.so`，生成动态库，以`.so`结尾
		3. 编译，`g++ -Iinclude -Lsrc -ltest main.cpp -o sharemain`
	3. 运行包含动态库的可执行文件时，需要指定动态库的位置，`LD_LIBRERY_PATH=src ./sharemain`
10. 多文件编译：`g++ -Wall  -Iinclude main.cpp  src/Circle.cpp src/Point.cpp -o main`

## 1.3 GDB调试器

### 1.3.1 启动调试
1. 只有在编译阶段添加了`-g`选项的程序才能被调试
2. 回车执行上一个命令
3. 有两种方法载入调试程序：
	1. **方法一**：`gdb exefilename`
	2. **方法二**：先进入gdb调试环境，直接敲`gdb`，然后使用file命令指定要调试的文件，具体为：`(gdb) file exefilename`
4. 按**q**退出调试

### 1.3.2 显示调试程序
使用`list(l)`命令显示调试程序源代码

### 1.3.3 添加断点
使用`break(b)`命令添加断点，有几种添加断点的方式：
1. `b func_name`：在指定函数上添加断点
2. `b row_num`：在指定的行添加断点
3. `b filename:row_num`：在指定文件的指定行添加断点
4. `b row_name if condition`：条件断点，如果条件为真则设置断点

### 1.3.4 查看断点
使用`info break(i b)`查看所有设置的断点

### 1.3.5 禁用和删除断点

使用`disbale Num`来禁用指定断点，注意这里的**Num**指的是断点的编号
使用`delete Num(d Num)`删除指定断点

### 1.3.6 运行程序
使用`start`或者`run(r)`运行程序

### 1.3.7 单步执行
使用`next(n)`执行下一步

### 1.3.8 跳入跳出函数
使用`step(s)`来跳入一个函数，使用`finish(f)`跳出一个函数

### 1.3.9 打印变量
使用`print(p) var`显示变量当前的值，或函数的返回值

### 1.3.10 设置观察点
使用`watch var`监控变量，当监控变量的值发生改变时，打印变量的改变

### 1.3.11 查看变量类型
使用`whatis var`查看变量的类型

### 1.3.12 在gdb中进入shell
使用`shell`命令在gdb中打开shell, 使用`exit`退回到gdb中

### 1.3.13 图形化调试界面
- 使用`tui enable`进入图形化界面，或者在启动时加上`-tui`参数，`gdb -tui a.out`
- `tui disable`退出图形化界面
- 图形化界面有四种窗口形式：**源代码src， 汇编asm，寄存器regs，命令行cmd**，其中默认的是命令行
	- 使用`layout name`切换窗口形式
	- 使用`focus name`切换当前窗口
	- 使用`winheight name +count/-count` 增大或减小窗口
	- `info win`显示当前窗口信息
	- `tui new-layout name [window weight]` 增加一个窗口




## 1.4 cmake

1. ==**cmake_minimun_required**==：指定cmake的最小版本要求
	- 语法：**cmake_minimum_required(VERSION versionNUMBER \[FATAL_ERROR])**
```cmake
	# cmake最小版本2.8.3
	cmake_minimum_required(VERSION 2.8.3)
```
2. **==projec(projectname)**==：指定工程名称，并可指定工程支持的语言
	- 语法：**project(projectname \[CXX] \[C]\[JAVA])**
```cmake
	#指定工程名为HELLOWORLD
	project(HELLOWORLD)
```
3. ==**set**==：显式定义变量
	- 语法**set(VAR \[VALUE] \[CACHE TYPE DOCSTRING\[FORCE]])**
```cmake
	#定义SRC变量，其值为main.cpp hello.cpp
	set(SRC main.cpp hello.cpp)
```
4. ==**include_directories**==：向工程添加多个特定头文件搜索路径，相当于指定g++编译期的-I参数
	- 语法：**include_directories(\[AFTER] \[BEFORE]\[SYSTEM] dir1 dir2...)**
```cmake
	# 将/usr/include/myfolder ./include 添加到头文件搜索路径
	include_directories(/usr/lib/myfolder ./inlcude)
```
5. ==**link_directories **==：向工程添加多个库文件搜索路径
	- 语法：**link_directories (dir1 dir2...)**
```cmake
	#将/usr/lib/myfolder ./lib 添加到库文件搜索路径
	link_directories(/usr/lib/myfolder ./lib)
```
6. ==**add_library**==：生成库文件
	- 语法：**add_library (libname \[STATIC|SHARED|MODULE]\[EXCLUDE_FROM_ALL] source1 source2...**
```cmake
	#通过变量SRC生成libhello.so共享库
	add_library(libhello.so ${SRC})
```
7. ==**add_compile_options**==：添加编译参数

- 语法：**add_compile_options (\<option>...)**

```cmake
	#添加编译参数 -Wall -std=c++11 -o2
	add_compile_options(-Wall -std=c++11 -o2)
```
8. ==**add_executable**==：生成可执行文件

- 语法：**add_executable (exename source1 source2...)**

```cmake
	#编译hello.cpp生成hello
	add_executable(hello hello.cpp)
```

9. **==target_link_libraries==**：为target添加需要链接的共享库
